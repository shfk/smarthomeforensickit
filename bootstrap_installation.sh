#!/usr/bin/env bash

# https://stackoverflow.com/questions/59895/how-to-get-the-source-directory-of-a-bash-script-from-within-the-script-itself
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

SETUP_DONE=".setup_done"

function setup() {
    if [ ! -e $SETUP_DONE ]; then
        sudo ./scripts/setup_raspi.sh install
        touch $SETUP_DONE
    else
        echo "already installed. delete $SETUP_DONE before running this script again, if you really want to run the installation again!"
    fi
}

cd ${DIR} && setup
