import datetime
import socket
from time import time
import logging

from postprocessing.db_wrapper.fqdn_manager import FQDNManager

MAX_AGE_FQDN_CACHE = 3600 * 24
logger = logging.getLogger(__name__)


class FQDNCache:
    def __init__(self):
        # ipadress: {stamp_updated: datetime, fqdn: str}
        self.cache = {}

    @staticmethod
    def fetch_fqdn(ip_address) -> str:
        logger.info('fetching fqdn for %s' % ip_address)
        time_before = time()
        fqdn = socket.getfqdn(ip_address)
        logger.debug('fetching took %ss' % (time() - time_before))
        return fqdn

    @staticmethod
    def fetch_fqdn_db(ip_address, db_wrapper: FQDNManager) -> dict:
        logger.info('getting fqdn from db: %s' % ip_address)
        fqdn_db = db_wrapper.get_fqdn(ip_address)
        if not fqdn_db:
            return False
        
        _, ip_address, stamp_updated, _fqdn = fqdn_db

        fqdn = {}
        fqdn['fqdn'] = _fqdn
        fqdn['stamp_updated'] = stamp_updated
        fqdn['ip_address'] = ip_address
        return fqdn

    def update_or_create_fqdn_db(self, fqdn: dict, fqdn_db: dict, db_wrapper: FQDNManager) -> None:
        if not fqdn:
            logger.debug('no fqdn!')
            return

        if fqdn_db:
            if fqdn['fqdn'] != fqdn_db['fqdn']:
                logger.info('different to db version, new entry')
                db_wrapper.store_fqdn(fqdn)
                return
            logger.debug('still the same in db, updating....')
            db_wrapper.touch_fqdn(fqdn_db, fqdn['stamp_updated'])
        else:
            logger.info('storing new fqdn: %s' % fqdn['ip_address'])
            db_wrapper.store_fqdn(fqdn)

    def get_fqdn(self, ip_address, db_wrapper: FQDNManager) -> dict:
        fqdn_cache = self.cache.get(ip_address, False)
        if fqdn_cache:
            if (datetime.datetime.now() - fqdn_cache['stamp_updated']).total_seconds() < MAX_AGE_FQDN_CACHE:
                return self.cache[ip_address]

        logger.debug('not in local cache')

        fqdn_db = FQDNCache.fetch_fqdn_db(ip_address, db_wrapper)
        if fqdn_db:
            if (datetime.datetime.now() - fqdn_db['stamp_updated']).total_seconds() < MAX_AGE_FQDN_CACHE:
                # setup local cache from db
                self.cache[ip_address] = {}
                self.cache[ip_address]['stamp_updated'] = fqdn_db['stamp_updated']
                self.cache[ip_address]['fqdn'] = fqdn_db['fqdn']
                self.cache[ip_address]['ip_address'] = fqdn_db['ip_address']
                return fqdn_db

        fqdn = self.fetch_fqdn(ip_address)
        logger.debug('updating on db')

        self.cache[ip_address] = {}
        self.cache[ip_address]['stamp_updated'] = datetime.datetime.now()
        self.cache[ip_address]['fqdn'] = fqdn
        self.cache[ip_address]['ip_address'] = ip_address

        self.update_or_create_fqdn_db(self.cache[ip_address], fqdn_db, db_wrapper)


        return self.cache[ip_address]['fqdn']
