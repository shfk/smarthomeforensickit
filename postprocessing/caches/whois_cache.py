import datetime
import logging
from time import time

from ipwhois import IPWhois, IPDefinedError

from postprocessing.db_wrapper.whois_manager import WhoisManager

logger = logging.getLogger(__name__)

MAX_AGE_WHOIS_CACHE = 3600 * 24 * 7


class WhoisCache:
    def __init__(self):
        self.cache = {}

    @staticmethod
    def fetch_whois_rdap(ip_address) -> (bool, bool):
        logger.info('fetching rdap for %s' % ip_address)
        try:
            whois = IPWhois(ip_address)
            time_before = time()
            data = whois.lookup_rdap()
            logger.debug('fetching took %ss' % (time() - time_before))

        except IPDefinedError:
            return False, {}

        except Exception as e:
            logger.error(e)
            return False, {}

        whois = {}
        whois['asn_cidr'] = data['asn_cidr']
        whois['asn_country_code'] = data['asn_country_code']
        if not data.get('asn_date', False):
            whois['asn_date'] = '1970-01-01'
        else:
            whois['asn_date'] = data['asn_date']
        whois['asn_description'] = data['asn_description']
        whois['network_start_address'] = data['network']['start_address']
        whois['network_end_address'] = data['network']['end_address']
        logger.debug('fetched %s' % whois)

        return True, whois

    @staticmethod
    def fetch_whois_db(ip_address, db_wrapper: WhoisManager):
        whois_db = db_wrapper.get_whois(ip_address)
        if not whois_db:
            return False

        _, stamp_updated, asn_cidr, asn_country_code, asn_date, asn_description, network_start_address, network_end_address, network_ip_version = whois_db

        asn_date: datetime.datetime 

        whois = {}
        whois['asn_cidr'] = asn_cidr
        whois['asn_country_code'] = asn_country_code
        whois['asn_date'] = asn_date.strftime('%Y-%m-%d') #  '2004-04-14'
        whois['asn_description'] = asn_description
        whois['network_start_address'] = network_start_address
        whois['network_end_address'] = network_end_address
        whois['stamp_updated'] = stamp_updated
        return whois

    def update_or_create_whois_db(self, whois, whois_db, db_wrapper: WhoisManager):
        if whois_db:
            # compare entry
            for key in ['asn_cidr',
                        'asn_country_code',
                        'asn_date',
                        'asn_description',
                        'network_start_address',
                        'network_end_address']:
                if whois[key] != whois_db[key]:
                    logger.info('different to db version, new entry')
                    db_wrapper.store_whois(whois)
                    return
            logger.debug('still the same in db, updating....')
            db_wrapper.touch_whois(whois_db, whois['stamp_updated'])
        else:
            db_wrapper.store_whois(whois)

    def get_whois(self, ip_address, db_wrapper: WhoisManager):
        logger.debug('getting whois for %s' % ip_address)

        whois_cache = self.cache.get(ip_address, False)
        if whois_cache:
            if (datetime.datetime.now() - whois_cache['stamp_updated']).total_seconds() < MAX_AGE_WHOIS_CACHE:
                return self.cache[ip_address]
        logger.debug('not in local cache')

        # ask the db if we dont have it in cache
        whois_db = WhoisCache.fetch_whois_db(ip_address, db_wrapper)
        if whois_db:
            if (datetime.datetime.now() - whois_db['stamp_updated']).total_seconds() < MAX_AGE_WHOIS_CACHE:
                # setup local cache from db
                self.cache[ip_address] = whois_db
                return whois_db

        logger.debug('not in db')

        success, whois = self.fetch_whois_rdap(ip_address)

        # set local cache
        self.cache[ip_address] = whois
        self.cache[ip_address]['stamp_updated'] = datetime.datetime.now()

        if success:
            logger.info('updating on db: %s' % ip_address)
            self.update_or_create_whois_db(whois, whois_db, db_wrapper)
        else:
            logger.info('no whois for %s' % ip_address)

        return self.cache[ip_address]
