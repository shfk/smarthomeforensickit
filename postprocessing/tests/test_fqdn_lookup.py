import datetime
import unittest

from postprocessing.merge_metadata.fqdn_lookup import FQDNLookup


class TestWhoisLookup(unittest.TestCase):

    def test_get_belonging_whois_older_metadata(self):
        '''
            0
        1

        :return:
        '''
        oldest_ts = datetime.datetime.fromtimestamp(0)
        fqdn = 'fqdn'
        
        searched_ts = datetime.datetime.fromtimestamp(1)

        fqdn_lookup = FQDNLookup()

        fqdn_lookup.add('192.168.0.1', oldest_ts, fqdn)

        result = fqdn_lookup.get('192.168.0.1', searched_ts)

        self.assertEqual(result, fqdn)

