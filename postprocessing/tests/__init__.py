import logging
import sys
from lib.init_logging import init_logging
init_logging('debug')
logging.basicConfig(stream=sys.stderr)
