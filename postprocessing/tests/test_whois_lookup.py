import datetime
import unittest

from postprocessing.merge_metadata.whois_lookup import WhoisLookup



class TestWhoisLookup(unittest.TestCase):

    def test_get_belonging_whois_older_metadata(self):
        '''
            0
        1

        :return:
        '''
        oldest_ts = datetime.datetime.fromtimestamp(0)
        expected_countrycode = 'countrycode'
        expected_description = 'description'

        searched_ts = datetime.datetime.fromtimestamp(1)

        whois_lookup = WhoisLookup()

        whois_lookup.add('1.1.0.0/24', oldest_ts, expected_countrycode, expected_description)

        whois = whois_lookup.get('1.1.0.1', searched_ts)

        self.assertEqual(whois['asn_country_code'], expected_countrycode)
        self.assertEqual(whois['asn_description'], expected_description)


    def test_get_belonging_whois_younger_metadata(self):
        '''
        1
            2
        :return:
        '''
        younger_ts = datetime.datetime.fromtimestamp(2)
        expected_countrycode = 'countrycode'
        expected_description = 'description'

        searched_ts = datetime.datetime.fromtimestamp(1)

        whois_lookup = WhoisLookup()

        whois_lookup.add('1.1.0.0/24', younger_ts, expected_countrycode, expected_description)

        whois = whois_lookup.get('1.1.0.1', searched_ts)

        self.assertEqual(whois['asn_country_code'], expected_countrycode)
        self.assertEqual(whois['asn_description'], expected_description)


    def test_get_belonging_whois_older_and_younger_metadata(self):
        '''
            0
        1
            2
        :return: 
        '''
        oldest_ts = datetime.datetime.fromtimestamp(0)
        expected_countrycode = 'countrycode_0'
        expected_description = 'description_0'

        searched_ts = datetime.datetime.fromtimestamp(1)

        youngest_ts = datetime.datetime.fromtimestamp(2)
        wrong_countrycode = 'countrycode_2'
        wrong_description = 'description_2'

        whois_lookup = WhoisLookup()

        whois_lookup.add('1.1.0.0/24', oldest_ts, expected_countrycode, expected_description)
        whois_lookup.add('1.1.0.0/24', youngest_ts, wrong_countrycode, wrong_description)

        whois = whois_lookup.get('1.1.0.1', searched_ts)

        self.assertEqual(whois['asn_country_code'], expected_countrycode)
        self.assertEqual(whois['asn_description'], expected_description)

