import datetime
import logging

logger = logging.getLogger(__name__)

from postprocessing.merge_metadata._base_lookup import BaseLookup


class FQDNLookup(BaseLookup):
    def add(self, ip_address, stamp_updated, fqdn):
        logger.debug('adding fqdn for %s at %s' % (ip_address, stamp_updated))
        self._add(key=ip_address, timestamp=stamp_updated, payload=fqdn)

    def get(self, ip_address, timestamp: datetime.datetime):
        """
        get data based on key and timestamp, finding the corresponding timestamp in data
        
        :param ip_address: 
        :param timestamp: 
        :return: 
        """
        timestamped_data: dict = self.data.get(ip_address, False)
        if timestamped_data:
            return self._get_belonging_data(timestamped_data, timestamp)

    def __str__(self):
        return str(self.data)
