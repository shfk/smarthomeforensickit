import datetime
import ipaddress
import logging
from time import time, sleep
from typing import Union

from postprocessing.db_wrapper.fqdn_manager import FQDNManager
from postprocessing.db_wrapper.mergedata_status_table_manager import MergedataStatusTableManager
from postprocessing.db_wrapper.metadata_status_table_manager import MetadataStatusTableManager
from postprocessing.db_wrapper.whois_manager import WhoisManager
from postprocessing.merge_metadata.fqdn_lookup import FQDNLookup
from postprocessing.merge_metadata.whois_lookup import WhoisLookup

logger = logging.getLogger(__name__)

TRAFFIC_DIRECTIONS = dict(
    LOCAL_TRAFFIC=0,
    INBOUND_TRAFFIC=1,
    OUTBOUND_TRAFFIC=2
)


class MetadataMerger():
    """
    merge data with fqdn and hostnames from raw_data_table[x] to merged_data_table[x]
    """

    def __init__(self, database, user, password, host, raw_data_tables, merged_data_tables):
        self.db_mergedata_status_table_manager = MergedataStatusTableManager(database, user, password, host)
        self.db_metadata_status_manager = MetadataStatusTableManager(database, user, password, host)

        self.db_whois_manager = WhoisManager(database, user, password, host)
        self.db_fqdn_manager = FQDNManager(database, user, password, host)

        self.fqdn_lookup = FQDNLookup()
        self.whois_lookup = WhoisLookup()

        # self.merged_tables_data = {target_table: {source: source_table, latest_pk: 0}}
        self.merged_tables_data = self.init_merged_tables_data(raw_data_tables, merged_data_tables)

    def init_merged_tables_data(self, raw_data_tables, merged_data_tables):
        """
        set up lookup dict for source, target table and latest pk for that table

        :param raw_data_tables: 
        :param merged_data_tables: 
        :return: 
        """
        merged_tables_data = {}
        for idx, raw_data_table in enumerate(raw_data_tables):
            raw_data_table_name = raw_data_tables[idx]
            merged_data_table_name = merged_data_tables[idx]
            merged_tables_data[merged_data_table_name] = {
                'source': raw_data_table_name, 'latest_pk': 0}
        return merged_tables_data

    def init_latest_pk(self):
        '''
        fetch latest processed keys from metadata status table,
        update table with new source+target if neccessary

        :return:
        '''

        # lines from db to dict {tablename: pk}
        latest_pks_stored_lines = self.db_mergedata_status_table_manager.get_latest_pks()
        latest_pks_stored = {}
        # todo: should we keep track of destination here?
        for line in latest_pks_stored_lines:
            _, raw_data_table_name, merged_table_name, latest_pk = line
            latest_pks_stored[merged_table_name] = latest_pk

        # init tables + local vars
        for merged_table_name in self.merged_tables_data.keys():
            if latest_pks_stored.get(merged_table_name, False) is not False:
                self.merged_tables_data[merged_table_name]['latest_pk'] = latest_pks_stored[merged_table_name]
            else:
                logger.info('initializing new status for table %s' % merged_table_name)
                self.merged_tables_data[merged_table_name]['latest_pk'] = 0

                source_table = self.merged_tables_data[merged_table_name]['source']
                self.db_mergedata_status_table_manager.init_latest_pk(source_table, merged_table_name, latest_pk=0)

    def init_fqdn_lookup(self):
        for row in self.db_fqdn_manager.get_all_fqdn():
            _, ip_address, stamp_updated, fqdn = row
            self.fqdn_lookup.add(ip_address, stamp_updated, fqdn)
        logger.info('initialized fqdn lookup: %s keys', len(self.fqdn_lookup.data))

    def init_whois_lookup(self):
        for row in self.db_whois_manager.get_all_whois():
            (_, stamp_updated, asn_cidr, asn_country_code,
             asn_date, asn_description,
             network_start_address, network_end_address,
             network_ip_version) = row
            self.whois_lookup.add(asn_cidr, stamp_updated, asn_country_code, asn_description)
        logger.info('initialized whois lookup: %s keys', len(self.whois_lookup.data))

    def ip_is_local(self, ip_address: Union[ipaddress.IPv4Address, ipaddress.IPv6Address]):
        if ip_address.is_loopback or ip_address.is_link_local or ip_address.is_multicast or ip_address.is_private or ip_address.is_unspecified:
            return True
        return False

    def get_traffic_direction(self, ip_src, ip_dst):
        _ip_src = ipaddress.ip_address(ip_src)
        _ip_dst = ipaddress.ip_address(ip_dst)
        ip_src_is_local = self.ip_is_local(_ip_src)
        ip_dst_is_local = self.ip_is_local(_ip_dst)

        if ip_src_is_local and ip_dst_is_local:
            return TRAFFIC_DIRECTIONS['LOCAL_TRAFFIC']
        if ip_src_is_local and not ip_dst_is_local:
            return TRAFFIC_DIRECTIONS['OUTBOUND_TRAFFIC']
        else:
            return TRAFFIC_DIRECTIONS['INBOUND_TRAFFIC']

    def merge_metadata(self):
        SLEEP_TIME = 5 * 60

        self.init_latest_pk()

        fields = ['pk', 'ip_src', 'ip_dst', 'mac_src', 'mac_dst', 'port_src', 'port_dst', 'ip_proto', 'packets',
                  'bytes', 'stamp_inserted', 'country_ip_src', 'country_ip_dst']

        while True:
            # keep track of postprocessor to not run ahead
            pk_latest_collected_metadata = self.db_metadata_status_manager.get_latest_pks()

            self.init_fqdn_lookup()
            self.init_whois_lookup()

            for merged_table in self.merged_tables_data.keys():
                source_table = self.merged_tables_data[merged_table]['source']
                latest_pk = self.merged_tables_data[merged_table]['latest_pk']
                logger.info('processing source %s' % source_table)

                logger.info('merging from %s (where we stopped last time) to %s (the point to which we have metadata) - %s lines' % (latest_pk, pk_latest_collected_metadata[source_table], pk_latest_collected_metadata[source_table] - latest_pk))

                for chunk in self.db_mergedata_status_table_manager.get_raw_from_data_table(
                        source_table, fields, latest_pk,
                        to_pk=pk_latest_collected_metadata[source_table],
                        chunk_size=10000):

                    if not chunk:
                        logger.info('no chunk, breaking')
                        break
                    logger.info('processing %s lines' % len(chunk))

                    result_lines = []
                    latest_pk = 0
                    time_before = time()
                    for line in chunk:

                        src_pk = line[0]
                        ip_src = line[1]
                        ip_dst = line[2]
                        stamp_inserted = line[10]
                        if src_pk > latest_pk:
                            latest_pk = src_pk

                        logger.debug(
                            f'source pk: {src_pk}, ip_src: {ip_src}, ip_dst: {ip_dst}, stamp_inserted: {stamp_inserted}')

                        _whois_src = self.whois_lookup.get(ip_src, timestamp=stamp_inserted)
                        whois_asn_country_code_src = _whois_src.get('asn_country_code', None)
                        whois_asn_description_src = _whois_src.get('asn_description', None)

                        _whois_dst = self.whois_lookup.get(ip_dst, timestamp=stamp_inserted)
                        whois_asn_country_code_dst = _whois_dst.get('asn_country_code', None)
                        whois_asn_description_dst = _whois_dst.get('asn_description', None)

                        fqdn_src = self.fqdn_lookup.get(ip_src, timestamp=stamp_inserted)
                        fqdn_dst = self.fqdn_lookup.get(ip_dst, timestamp=stamp_inserted)

                        traffic_direction = self.get_traffic_direction(ip_src, ip_dst)

                        fields_merged = (
                            *line, whois_asn_country_code_src, whois_asn_description_src, whois_asn_country_code_dst,
                            whois_asn_description_dst, fqdn_src, fqdn_dst, traffic_direction)
                        result_lines.append(fields_merged)

                    logger.info('adding %s lines to %s' % (len(result_lines), merged_table))
                    self.db_mergedata_status_table_manager.insert_batch(result_lines, merged_table)

                    # update latest pk on table and local
                    self.merged_tables_data[merged_table]['latest_pk'] = latest_pk
                    self.db_mergedata_status_table_manager.update_latest_pk(source_table_name=source_table,
                                                                            destination_table_name=merged_table,
                                                                            latest_pk=latest_pk)
                    logger.info('whole run took %ss' % (time() - time_before))
            logger.info('%s: sleeping for %ss' % (datetime.datetime.now(), SLEEP_TIME))
            sleep(SLEEP_TIME)
