import datetime
import ipaddress
import logging
from typing import Union

from postprocessing.merge_metadata._base_lookup import BaseLookup

logger = logging.getLogger(__name__)


class WhoisLookup(BaseLookup):
    def __init__(self):
        super().__init__()
        self.data_by_ip_address = {}  # address based data for faster lookup

    def add(self, cidr, stamp_updated, whois_country_code, whois_description):
        logger.debug('adding whois for %s at %s' % (cidr, stamp_updated))
        _cidr = ipaddress.ip_network(cidr)
        payload = dict(asn_country_code=whois_country_code,
                       asn_description=whois_description)
        self._add(_cidr, stamp_updated, payload)

    def is_local(self, ip_address: Union[ipaddress.IPv4Address, ipaddress.IPv6Address]):
        if ip_address.is_loopback or ip_address.is_link_local or ip_address.is_multicast or ip_address.is_private or ip_address.is_unspecified:
            return True
        return False

    def _get_timestamped_whois(self, ip_address):
        if self.data_by_ip_address.get(ip_address):
            return self.data_by_ip_address[ip_address]

        logger.debug('not in ip address cache')

        # for faster lookup, cache CIDR based dict to ip based dict

        _ip_address = ipaddress.ip_address(ip_address)
        if self.is_local(_ip_address):
            return False

        for _cidr in self.data.keys():
            if _ip_address in _cidr:
                logger.debug('ip %s in %s -> %s' % (_ip_address, _cidr,self.data[_cidr]))
                self.data_by_ip_address[ip_address] = self.data[_cidr]
                return self.data_by_ip_address[ip_address]
        return False

    def get(self, ip_address, timestamp: datetime.datetime):
        _ip_address = ipaddress.ip_address(ip_address)
        if self.is_local(_ip_address):
            # local addresses dont have whois
            return {}

        logger.debug('getting whois for %s, %s' % (ip_address, timestamp))
        timestamped_data: dict = self._get_timestamped_whois(ip_address)
        if timestamped_data:
            return self._get_belonging_data(timestamped_data, timestamp)
        logger.debug('no whois found for %s' % ip_address)
        return {}
