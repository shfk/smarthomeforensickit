import datetime


class BaseLookup:
    def __init__(self):
        self.data = {}

    def _add(self, key, timestamp, payload):
        if not self.data.get(key, False):
            self.data[key] = {}
        self.data[key][timestamp] = payload

    def _get_belonging_data(self, timestamped_payload, data_timestamp: datetime.datetime):
        """
        since metadata for an ip might change over time, we need to match the collected timestamp to the metadata timestamp:

        based on the collected-data-timestamp, return either the youngest metadata-timestamp older than the searched timestamp, or if there is nothing, the oldest.

        collected data      |   metadata
        for one ip address      timestamps
        timestamps
                     1
                     2
                             3
                     4
                     5
                             6
                     7
                             8
                     9

        matches as follows:
        1 -> 3
        2 -> 3
        for timestamp 1 and 2, there was no metadata. (the metadata was fetched at time 3)
        so we need to use the oldest one in that case.

        4 -> 3
        5 -> 3
        timestamp 4 and 5 use the next younger timestamp (3).


        7 -> 6
        9 -> 8
        same for the rest: 7 uses metadata from 6, 9 uses metadata from 8.

        :param ip_address:
        :param timestamp:
        :return:
        """

        oldest_timestamp = None
        youngest_older_than_searched_timestamp = None
        for timestamp in timestamped_payload.keys():
            # find oldest timestamp
            if not oldest_timestamp or timestamp < oldest_timestamp:
                oldest_timestamp = timestamp

            # find youngest timestamp which is older than the searched ts
            if timestamp < timestamp:
                if not youngest_older_than_searched_timestamp or youngest_older_than_searched_timestamp < timestamp:
                    youngest_older_than_searched_timestamp = timestamp

        if not youngest_older_than_searched_timestamp:
            return timestamped_payload[oldest_timestamp]

        return timestamped_payload[youngest_older_than_searched_timestamp]

