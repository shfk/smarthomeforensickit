import datetime
from time import time, sleep
import ipaddress

from caches.fqdn_cache import FQDNCache
from caches.whois_cache import WhoisCache

import logging

from postprocessing.db_wrapper.fqdn_manager import FQDNManager
from postprocessing.db_wrapper.metadata_status_table_manager import MetadataStatusTableManager
from postprocessing.db_wrapper.whois_manager import WhoisManager

logger = logging.getLogger(__name__)


class PostProcessor():
    def __init__(self, database, user, password, host, tables):
        self.fqdn_cache = FQDNCache()
        self.whois_cache = WhoisCache()
        self.db_metadata_status_manager = MetadataStatusTableManager(database, user, password, host)
        self.db_whois_manager = WhoisManager(database, user, password, host)
        self.db_fqdn_manager = FQDNManager(database, user, password, host)

        self.metadata_tables_latest_pk = {table: 0 for table in tables}

    def log_ip_metadata(self, ip):
        self.whois_cache.get_whois(ip, self.db_whois_manager)
        self.fqdn_cache.get_fqdn(ip, self.db_fqdn_manager)
        return

    def init_latest_pk(self):
        '''
        fetch latest processed keys from metadata status table

        :return:
        '''
        # lines from db to dict {tablename: pk}
        latest_pks_stored = self.db_metadata_status_manager.get_latest_pks()

        # init tables + local vars
        for table_name in self.metadata_tables_latest_pk.keys():
            if latest_pks_stored.get(table_name, False) is not False:
                self.metadata_tables_latest_pk[table_name] = latest_pks_stored[table_name]
            else:
                logger.info('initializing new status for table %s' % table_name)
                self.db_metadata_status_manager.init_latest_pk(table_name, 0)
                self.metadata_tables_latest_pk[table_name] = 0

    def run(self):
        logger.info('starting!')
        SLEEP_TIME = 5
        self.init_latest_pk()

        while True:
            for table in self.metadata_tables_latest_pk.keys():
                latest_pk = self.metadata_tables_latest_pk[table]

                for (new_latest_pk, ips) in self.db_metadata_status_manager.get_ips_from_data_table(table, latest_pk, chunk_size=1000):
                    # iterate over chunk of ips
                    logger.info('processing chunk: %s ips' % len(ips))
                    if new_latest_pk == latest_pk:
                        # skip if nothing happened
                        continue

                    time_before = time()
                    if ips:
                        for ip in ips:
                            logger.info('processing %s' % ip)
                            self.log_ip_metadata(ip)

                    self.db_metadata_status_manager.metadata_status_update_latest_pk(table, new_latest_pk)
                    self.metadata_tables_latest_pk[table] = new_latest_pk
                    logger.info('whole run took %ss' % (time() - time_before))

            logger.info('%s: sleeping for %ss' % (datetime.datetime.now(), SLEEP_TIME))
            sleep(SLEEP_TIME)



