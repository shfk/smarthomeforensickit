import logging

from postprocessing.db_wrapper import DBWrapper, FQDN_PG_TABLE

logger = logging.getLogger(__name__)


class FQDNManager(DBWrapper):
    def get_all_fqdn(self):
        query = "select * from {pg_table} " \
                "order by stamp_updated desc".format(pg_table=FQDN_PG_TABLE)

        with self.cursor() as cur:
            self.run_query(cur, query)
            fqdn = cur.fetchall()

        return fqdn

    def get_fqdn(self, ip_address):
        query = "select * from {pg_table} " \
                "where ip_address = %s " \
                "order by stamp_updated desc".format(pg_table=FQDN_PG_TABLE)

        with self.cursor() as cur:
            self.run_query(cur, query, (ip_address,))
            fqdn = cur.fetchone()

        return fqdn

    def store_fqdn(self, fqdn: dict):
        insert = 'insert into {pg_table} ' \
                 '(ip_address, stamp_updated, fqdn) ' \
                 'values (%s, %s, %s)'.format(pg_table=FQDN_PG_TABLE)

        with self.cursor() as cur:
            self.run_query(cur, insert, (fqdn['ip_address'], fqdn['stamp_updated'], fqdn['fqdn'],))

        logger.debug('added fqdn for %s: %s' % (fqdn['ip_address'], fqdn))
        pass

    def touch_fqdn(self, fqdn_db: dict, new_timestamp):
        logger.debug('updating timestamp for %s' % fqdn_db)
        update_query = 'update {pg_table} ' \
                       'set stamp_updated = %s ' \
                       'where ip_address = %s ' \
                       'and stamp_updated = %s'.format(pg_table=FQDN_PG_TABLE)

        with self.cursor() as cur:
            self.run_query(cur, update_query,
                           (new_timestamp,
                            fqdn_db['ip_address'],
                            fqdn_db['stamp_updated']), )
