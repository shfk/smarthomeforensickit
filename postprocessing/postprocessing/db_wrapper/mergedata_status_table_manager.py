import logging

from psycopg2.extras import execute_batch

from postprocessing.db_wrapper import DBWrapper, MERGEDATA_STATUS_TABLE

logger = logging.getLogger(__name__)


class MergedataStatusTableManager(DBWrapper):
    def get_latest_pks(self):
        query = "SELECT * FROM {MERGEDATA_STATUS_TABLE}".format(
            MERGEDATA_STATUS_TABLE=MERGEDATA_STATUS_TABLE)
        with self.cursor() as cur:
            self.run_query(cur, query)
            return cur.fetchall()

    def init_latest_pk(self, source_table_name, destination_table_name, latest_pk=0):
        query = "INSERT INTO {MERGEDATA_STATUS_TABLE}(source_table_name, destination_table_name, latest_pk) VALUES (%s, %s, %s)".format(
            MERGEDATA_STATUS_TABLE=MERGEDATA_STATUS_TABLE)
        with self.cursor() as cur:
            self.run_query(cur, query, (source_table_name, destination_table_name, latest_pk))

    def update_latest_pk(self, source_table_name, destination_table_name, latest_pk):
        logger.info('setting new latest pk of %s to %s' % (destination_table_name, latest_pk))
        query = "UPDATE {MERGEDATA_STATUS_TABLE} " \
                "SET latest_pk = %s " \
                "WHERE source_table_name = %s " \
                "AND destination_table_name = %s" \
                "".format(MERGEDATA_STATUS_TABLE=MERGEDATA_STATUS_TABLE)

        with self.cursor() as cur:
            self.run_query(cur, query, (latest_pk, source_table_name, destination_table_name))

    def insert_batch(self, lines, destination_table_name):
        with self.cursor() as cur:
            columns = 'source_pk,ip_src,ip_dst,mac_src,mac_dst,port_src,port_dst,ip_proto,packets,bytes,stamp_inserted,country_ip_src,country_ip_dst,whois_asn_country_code_src,whois_asn_description_src,whois_asn_country_code_dst,whois_asn_description_dst,fqdn_src,fqdn_dst,traffic_direction'
            query = 'INSERT INTO {MERGE_TABLE} ({COLUMNS}) VALUES (' \
                    '%s,%s,%s,%s,%s,' \
                    '%s,%s,%s,%s,%s,' \
                    '%s,%s,%s,%s,%s,' \
                    '%s,%s,%s,%s,%s)'.format(
                MERGE_TABLE=destination_table_name, COLUMNS=columns)

            sqls = [cur.mogrify(query, args) for args in lines]

            cur.execute(b";".join(sqls))

            #execute_batch(cur, query, lines)
