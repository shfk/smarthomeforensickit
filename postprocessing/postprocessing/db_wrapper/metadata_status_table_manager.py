import logging

from postprocessing.db_wrapper import DBWrapper, METADATA_STATUS_TABLE

logger = logging.getLogger(__name__)


class MetadataStatusTableManager(DBWrapper):
    def get_latest_pks(self):
        query = "SELECT * FROM {METADATA_STATUS_TABLE}".format(
            METADATA_STATUS_TABLE=METADATA_STATUS_TABLE)
        with self.cursor() as cur:
            self.run_query(cur, query)
        
            latest_pks_stored = {}
            for line in cur.fetchall():
                _, table_name, latest_pk = line
                latest_pks_stored[table_name] = latest_pk

        return latest_pks_stored

    def init_latest_pk(self, table_name, latest_pk=0):
        query = "INSERT INTO {METADATA_STATUS_TABLE}(table_name, latest_pk) VALUES (%s, %s)".format(
            METADATA_STATUS_TABLE=METADATA_STATUS_TABLE)
        with self.cursor() as cur:
            self.run_query(cur, query, (table_name, latest_pk))

    def metadata_status_update_latest_pk(self, table, latest_pk):
        query = "UPDATE {METADATA_STATUS_TABLE} " \
                "SET latest_pk = %s " \
                "WHERE table_name = %s".format(METADATA_STATUS_TABLE=METADATA_STATUS_TABLE)
        with self.cursor() as cur:
            self.run_query(cur, query, (latest_pk, table))
