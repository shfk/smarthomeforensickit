import datetime
import logging
from time import time
from typing import List, Tuple

import psycopg2
from psycopg2._psycopg import cursor

METADATA_STATUS_TABLE = "metadata_status"
MERGEDATA_STATUS_TABLE = 'mergedata_status'
WHOIS_PG_TABLE = 'whois'
FQDN_PG_TABLE = 'fqdn'

logger = logging.getLogger(__name__)


class DBWrapper:
    def __init__(self, database, user, password, host):
        self.database = database
        self.user = user
        self.password = password
        self.host = host
        pass

    @staticmethod
    def run_query(cur: cursor, query: str, args: Tuple = None):
        parsed_query = cur.mogrify(query, args)
        logger.debug('running query: %s' % parsed_query)
        time_before = time()
        cur.execute(parsed_query)
        logger.debug('took %ss' % (time() - time_before))

    def cursor(self, name=None, autocommit=True):
        with psycopg2.connect(database=self.database,
                              user=self.user,
                              password=self.password,
                              host=self.host,
                              application_name="enricher") as connection:
            connection.autocommit = autocommit
            return connection.cursor(name)

    def get_raw_from_data_table(self, table, fields: List[str], from_pk, chunk_size=1000, to_pk=None) -> List[List]:
        with self.cursor(name=f'read_raw_{time()}', autocommit=False) as cur:
            logger.debug("using cursor with itersize %s" % chunk_size)
            cur.itersize = chunk_size
            query = "SELECT {fields} " \
                    "FROM {table} " \
                    "WHERE pk > %s ".format(fields=', '.join(fields), table=table)
            if to_pk is None:
                query += " ORDER BY pk ASC"
                self.run_query(cur, query, (from_pk,))
            else:
                query += " and pk <= %s"
                query += " ORDER BY pk ASC"
                self.run_query(cur, query, (from_pk, to_pk))

            time_before = time()

            while True:
                result = cur.fetchmany(chunk_size)
                logger.debug('get_unprocessed_entries: fetchmany for %s from %s rows took %ss' % (
                    len(result), cur.rowcount, time() - time_before))
                if not result:
                    break
                yield result
            yield None

    def get_ips_from_data_table(self, table, from_pk, chunk_size=1000) -> (int, int, List[str]):
        '''
        get a block of ip addresses in range from_pk to from_pk+chunk_size.
        also return the highest pk in the block.

        :return: max_pk: int, ips: List[str]
        '''
        fields = ['pk', 'ip_src', 'ip_dst']
        for result in self.get_raw_from_data_table(table, fields, from_pk, chunk_size):
            if result:
                logger.debug('processing %s rows' % len(result))
                ips = set([tuple[1] for tuple in result] + [tuple[2] for tuple in result])
                max_pk = max([tuple[0] for tuple in result])
                yield max_pk, ips
        return None, None
