import logging

from postprocessing.db_wrapper import DBWrapper, WHOIS_PG_TABLE

logger = logging.getLogger(__name__)


class WhoisManager(DBWrapper):
    def get_all_whois(self):
        query = "select * from {pg_table} " \
                "order by stamp_updated desc".format(pg_table=WHOIS_PG_TABLE)
        with self.cursor() as cur:
            self.run_query(cur, query)
            whois = cur.fetchall()
        return whois

    def get_whois(self, ip):
        """
        get the newest whois entry for this ip from the db

        :param ip:
        :return:
        """
        query = "select * from {pg_table} " \
                "where %s << asn_cidr " \
                "order by stamp_updated desc".format(pg_table=WHOIS_PG_TABLE)

        with self.cursor() as cur:
            self.run_query(cur, query, (ip,))
            whois = cur.fetchone()

        return whois

    def store_whois(self, whois):
        keys = whois.keys()
        columns = ','.join(keys)
        values = tuple(whois[key] for key in keys)
        insert = 'insert into {pg_table} ' \
                 '({columns}) ' \
                 'values %s ' \
                 'RETURNING pk'.format(columns=columns,
                                       pg_table=WHOIS_PG_TABLE)

        with self.cursor() as cur:
            self.run_query(cur, insert, (values,))
            primary_key = cur.fetchone()

        logger.debug('added whois for %s: %s' % (whois['asn_cidr'], whois))

    def touch_whois(self, whois_db: dict, new_timestamp):
        """
        update whois timestamp in db

        :param whois:
        :return:
        """

        logger.debug('updating timestamp for %s' % whois_db)
        update_query = 'update {pg_table} ' \
                       'set stamp_updated = %s ' \
                       'where asn_cidr = %s ' \
                       'and stamp_updated = %s'.format(pg_table=WHOIS_PG_TABLE)
        with self.cursor() as cur:
            self.run_query(cur, update_query,
                           (new_timestamp,
                            whois_db['asn_cidr'],
                            whois_db['stamp_updated']), )
