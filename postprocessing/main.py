import os

import click

from postprocessing.get_metadata.postprocessor import PostProcessor
from postprocessing.merge_metadata.metadata_merger import MetadataMerger

PG_DATABASE = os.environ['POSTGRES_DB']
PG_HOST = os.environ['POSTGRES_HOST']
PG_USER = os.environ['POSTGRES_USER']
PG_PASSWORD = os.environ['POSTGRES_PASSWORD']
RAW_DATA_TABLES = [table for table in os.environ['RAW_DATA_TABLES'].split(',')]
MERGED_DATA_TABLES = [table for table in os.environ['MERGED_DATA_TABLES'].split(',')]

from lib.init_logging import init_logging

@click.group()
@click.option('--debug/--no-debug', default=False)
def cli(debug):
    if debug:
        init_logging(loglevel='debug')
    else:
        init_logging(loglevel='info')


@cli.command()
def run():
    dp = PostProcessor(PG_DATABASE, PG_USER, PG_PASSWORD, PG_HOST, RAW_DATA_TABLES)
    dp.run()


@cli.command()
def enrich():
    if len(MERGED_DATA_TABLES) != len(RAW_DATA_TABLES):
        print('MERGED_DATA_TABLES and RAW_DATA_TABLES should be the same amount!')
        exit(1)

    dp = MetadataMerger(PG_DATABASE, PG_USER, PG_PASSWORD, PG_HOST, RAW_DATA_TABLES, MERGED_DATA_TABLES)
    dp.merge_metadata()


cli()
