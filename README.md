# SmartHomeForensicKit

Das SmartHomeForensicKit (SHFK) ist ein Toolset zur Netzwerkanalyse.   
Dazu wird ein Raspberry Pi als Wlan AccessPoint konfiguriert, 
dessen Netzwerkverkehr gespeichert wird und in Grafana Boards visualisiert wird.

Um eine Idee zu bekommen, wie das nachher aussehen kann, findest du [hier](https://shfk.labs.datenfreunde.net/dashboard/snapshot/iAh7kXP8Sm7Xn96Rr53HWc3CWd01bY7s) einen Grafana Snapshot eines Boards.


## Benötigte Hardware

- ein Raspberry Pi 3 oder 4, mit microSD Karte und Netzteil.
- ein Netzwerkkabel, um den Raspberry mit einem Router zu verbinden


## Installation

Wie du das SHFK auf den Raspberry installierst steht in der
[Installationsanleitung](https://gitlab.com/shfk/smarthomeforensickit/-/wikis/SHFK-installieren).


## SHFK benutzen

Nach der Installation lies [hier](https://gitlab.com/shfk/smarthomeforensickit/-/wikis/SHFK-benutzen) wie du das Toolset benutzt.

## Ähnliche Projekte

#### c't-Raspion

[c't-Raspion](https://www.heise.de/ct/artikel/c-t-Raspion-Projektseite-4606645.html) kommt unter anderem mit Pi-hole, ntopng und MitmProxy. Wenn du herausgefunden hast, das sich ein Gerät merkwürdig verhält, und du genauer reinschauen möchtest, lohnt sich viellicht ein Blick auf dieses Projekt.

## Einen neuen Release erstellen

Voraussetzung: Du hast das Repository in deinen GitLab Account geklont/geforkt.

Releases in GitLab sind an git Tags gekoppelt, Tags können mit [bump2version](https://github.com/c4urself/bump2version) gemanaged werden.

    bump2version [patch|minor|major] --verbose  # version bumpen, commiten, und commit taggen
    git push --follow-tags  # commits und tags pushen 

Jetzt fehlen noch die Release Notes, ohne die kein Release erstellt weren kann.
In GitLab unter  `Repository` -> `Tags` -> [vX.X.X] -> `Edit release notes` klicken. 

Nach Eingabe deiner Release Notes und einem Klick auf Save wird dein Release erstellt.


