#!/bin/sh

latest_release="v0.0.7"
shfk_name="smarthomeforensickit-${latest_release}"

check_system() {
    if [ -f /etc/os-release ]; then
        . /etc/os-release
        if [ "$ID" = "raspbian" ]; then
            return 0
        fi
    fi
    echo "This script must be executed on a Raspbian distribution. Exiting script."
    exit 1
}

download_shfk() {

    url="https://gitlab.com/shfk/smarthomeforensickit/-/archive/${latest_release}/${shfk_name}.tar.gz"

    echo "Donwloading ${shfk_name}"

    if curl --silent -o "${PWD}/${shfk_name}.tar.gz" -L "$url"; then
        echo "File downloaded and ready to extract"
        return 0
    else
        echo "Can't download SHFK. Exiting script."
    fi
    exit 1
}

install_shfk() {

    echo "Installing to ${PWD}/${shfk_name}"

    if tar -xf "${shfk_name}.tar.gz"; then
        if [[ -d "${PWD}/${shfk_name}" ]]; then

            echo "Removing the file"
            rm -f "${PWD}/${shfk_name}.tar.gz"

            echo "Rename folder.."
            if mv "${PWD}/$shfk_name" "${PWD}/shfk"; then
                echo "cd shfk"
                cd shfk  || exit 1

                echo "Running installation script"
                echo "sudo ./bootstrap_installation.sh"
                sudo ./bootstrap_installation.sh
                return 0
            fi
        else
            echo "Directory not found. Exiting script."
        fi
    else
        echo "Archive seems to be corrupted. Exiting script."
    fi
    exit 1
}

echo "---------------------------"
echo " SHFK installation script  "
echo "---------------------------"

# Ensure we are on a raspbian distri right now
check_system

# Download the tools as tar.gz
download_shfk

# Extract the tools. Then call the installation script.
install_shfk

exit 0

