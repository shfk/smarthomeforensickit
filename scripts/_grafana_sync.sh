#!/usr/bin/env bash


function _grafana_post_dashboard() {
    local HOST="${1}"
    local USER="${2}"
    local PASSWORD="${3}"
    local FILE_PATH="${4}"

    curl -H "Content-Type: application/json" --data @${FILE_PATH} \
    "${HOST}api/dashboards/db" -u ${USER}:${PASSWORD}
}


function _grafana_restore() {
    local GRAFANA_HOST="${1}"
    local USER="${2}"
    local PASSWORD="${3}"

    # grafana sources have variables, so we need to replace them
    _write_template grafana/sources/sources.json.template /tmp/grafana_sources.json
    curl -H "Content-Type: application/json" --data @/tmp/grafana_sources.json \
    "${GRAFANA_HOST}api/datasources" -u ${USER}:${PASSWORD}


    for f in grafana/dashboards/*
    do
        _grafana_post_dashboard ${GRAFANA_HOST} ${USER} ${PASSWORD} ${f}
    done
    _restore_main_dashboard "${GRAFANA_HOST}" "${USER}" "${PASSWORD}"

}

function _grafana_backup() {
    local HOST="${1}"
    local USER="${2}"
    local PASSWORD="${3}"
    if ! hash jq 2>/dev/null; then
        echo "you need to have jq installed for this!"
        echo "https://stedolan.github.io/jq/"
        exit 1
    fi
    #curl -s "${GRAFANA_HOST}api/datasources" -u  ${GRAFANA_ADMIN_USER}:${GRAFANA_ADMIN_PASSWORD} | jq -c -M '.[]' > grafana/sources/sources.json

    for dashboard in $(curl -s "${HOST}api/search?query=&" -u ${USER}:${PASSWORD} | jq '.' | grep -i uri|awk -F '"uri": "' '{ print $2 }'|awk -F '"' '{print $1 }'); do
        echo "pull ${dashboard}"
        curl -u ${USER}:${PASSWORD} -s "${HOST}api/dashboards/${dashboard}" | jq '.dashboard.id = null' | jq 'del(.meta)' | jq ' . + {"overwrite": true}'  > grafana/dashboards/$(echo ${dashboard}|sed 's,db/,,g').json
    done
}

function _restore_main_dashboard() {
  local HOST="${1}"
  local USER="${2}"
  local PASSWORD="${3}"

  local DASHBOARD="$(curl -s ${HOST}'api/search?query=&tag=home' -u ${USER}:${PASSWORD} | jq '.[] | .id' 2>/dev/null)"

  curl -X POST "${HOST}api/user/stars/dashboard/"${DASHBOARD} -u ${USER}:${PASSWORD}
  curl -X PUT "${HOST}api/org/preferences" -u ${USER}:${PASSWORD} -H "Content-Type: application/json" -d '{"homeDashboardId":'"$DASHBOARD}"
}
