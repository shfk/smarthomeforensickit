#!/usr/bin/env bash

set -e
#set -x

export DEBIAN_FRONTEND=noninteractive

function install() {
    apt-get update
    apt-get dist-upgrade -yq

    _install_docker

    # basic stuff
    apt-get -yq install \
        git \
        jq \
        vim \
        tmux \
        pmacct \
        dnsmasq \
        hostapd \
        postgresql-client \
        iptables-persistent

    sudo systemctl stop dnsmasq
    sudo systemctl stop hostapd
}

function _install_docker() {
    # docker

    echo "installing docker..."
    apt-get install \
        docker-compose \
        docker.io \
        -yq
    usermod -aG docker pi
}

function setup() {
    echo "configuring..."

    # unblock wifi
    rfkill unblock wlan

    cp files/etc_hostname /etc/hostname
    cp files/etc_hosts /etc/hosts

    # open a wifi
    ## https://www.raspberrypi.org/documentation/configuration/wireless/access-point.md
    _write_template files/etc_dhcpcd.conf.template /etc/dhcpcd.conf
    systemctl start dhcpcd

    _write_template files/etc_dnsmasq.conf.template /etc/dnsmasq.conf
    systemctl start dnsmasq

    _write_template files/etc_hostapd_hostapd.conf.template /etc/hostapd/hostapd.conf
    cp files/etc_default_hostapd /etc/default/hostapd

    systemctl unmask hostapd
    systemctl enable hostapd
    systemctl start hostapd

    # add routing
    cat files/etc_sysctl.conf_append >> /etc/sysctl.conf
    cp files/etc_iptables_rules.v4 /etc/iptables/rules.v4

    # pmacct
    mkdir -p /etc/pmacct
    _write_template files/etc_pmacct_pmacctd.conf.template /etc/pmacct/pmacctd.conf
    cp files/GeoLite2-Country.mmdb /etc/pmacct/GeoLite2-Country.mmdb

    cp files/etc_systemd_system_pmacctd.service /etc/systemd/system/pmacctd.service
    systemctl enable pmacctd.service
    systemctl start pmacctd.service
}

function start() {
    echo "starting..."
    sudo docker-compose up -d
}

function setup_grafana() {
    echo "waiting for grafana to start..."
    ./wait-for-it.sh --host=localhost --port=3000 -t 60

    sleep 20  # after the port is up, grafana is not ready. we have to wait a bit. :(

    local GRAFANA_HOST="http://localhost:3000/"
    _grafana_restore "${GRAFANA_HOST}" "${GRAFANA_ADMIN_USER}" "${GRAFANA_ADMIN_PASSWORD}"
}

function _write_template() {
    local _from=$1
    local _to=$2

    cp ${_from} ${_to}

    IFS="="
    while read -r name value
    do
        # dont process empty lines or comments
        if [[ -n "$name" ]] && [[ ! $name =~ ^#.* ]]
         then
            # escape values for sed
            value_escaped=$(echo "${value}" | sed -e 's/[\/&]/\\&/g')
            sed -i "s/SHFK__${name}/${value_escaped}/g" ${_to}
        fi
    done < scripts/shfk.conf
}


function run_function() {
    func=$1
    shift
    ${func} "$@"
}

USAGE="$0 COMMAND \n\
commands:\
    install                 -   install everything\
"


# set config vars
source scripts/_grafana_sync.sh
source scripts/shfk.conf

if [ "$1" == "install" ]; then
    install
    setup
    start
    setup_grafana
    echo ""
    echo ""
    echo "setup finished! you should reboot now. :)"
    echo "(just run 'sudo reboot'!)"

    exit 0
elif [ "$1" == "_function" ]; then
    shift
    run_function "$@"
    exit 0
else
    echo "${USAGE}"
    exit 0
fi
shift


