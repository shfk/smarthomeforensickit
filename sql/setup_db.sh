#!/usr/bin/env bash

# drops and recreates tables
# started by container on first run

set -x

username=${POSTGRES_USER}
password=${POSTGRES_PASSWORD}
db_name=${POSTGRES_DB}

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"


# create database
PGPASSWORD="${password}" psql --username="${username}" --dbname template1 <<SQL |\
DROP DATABASE ${db_name};
CREATE DATABASE ${db_name};
SQL


# create tables
PGPASSWORD="${password}" psql --username="${username}" --dbname ${db_name} -f ${DIR}/create_tables.pgsql


# setup permissions
PGPASSWORD="${password}" psql --username="${username}" --dbname ${db_name} <<SQL |\
CREATE USER ${username};
ALTER USER ${username} WITH PASSWORD '${password}';
GRANT SELECT, INSERT, UPDATE, DELETE ON whois TO ${username};
GRANT SELECT, INSERT, UPDATE, DELETE ON collected TO ${username};
GRANT SELECT, INSERT, UPDATE, DELETE ON proto TO ${username};
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO ${username};
SQL
