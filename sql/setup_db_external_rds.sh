#!/usr/bin/env bash

set -x

username=${POSTGRES_USER}
password=${POSTGRES_PASSWORD}
db_name=${POSTGRES_DB}
host=${POSTGRES_HOST}



DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

PGPASSWORD="${password}" psql --host=${host} --username="${username}" --dbname ${db_name} -f ${DIR}/create_tables.pgsql
