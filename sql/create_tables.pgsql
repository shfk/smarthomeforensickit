DROP TABLE IF EXISTS collected;
CREATE TABLE collected
(
    pk                         SERIAL PRIMARY KEY,
    mac_src                    macaddr                     NOT NULL DEFAULT '0:0:0:0:0:0',
    mac_dst                    macaddr                     NOT NULL DEFAULT '0:0:0:0:0:0',
    ip_src                     inet                        NOT NULL DEFAULT '0.0.0.0',
    ip_dst                     inet                        NOT NULL DEFAULT '0.0.0.0',
    port_src                   INT                         NOT NULL DEFAULT 0,
    port_dst                   INT                         NOT NULL DEFAULT 0,
    ip_proto                   SMALLINT                    NOT NULL DEFAULT 0,
    packets                    INT                         NOT NULL,
    bytes                      BIGINT                      NOT NULL,
    stamp_inserted             timestamp without time zone NOT NULL DEFAULT '0001-01-01 00:00:00',
    stamp_updated              timestamp without time zone,
    country_ip_src             CHAR(2)                              DEFAULT NULL,
    country_ip_dst             CHAR(2)                              DEFAULT NULL,
    timestamp_arrival          timestamp without time zone,
    timestamp_arrival_residual int
);

CREATE INDEX idx_stamp_inserted_1h ON collected_1h(stamp_inserted);
DROP TABLE IF EXISTS collected_1h;
CREATE TABLE collected_1h
(
    pk                         SERIAL PRIMARY KEY,
    mac_src                    macaddr                     NOT NULL DEFAULT '0:0:0:0:0:0',
    mac_dst                    macaddr                     NOT NULL DEFAULT '0:0:0:0:0:0',
    ip_src                     inet                        NOT NULL DEFAULT '0.0.0.0',
    ip_dst                     inet                        NOT NULL DEFAULT '0.0.0.0',
    port_src                   INT                         NOT NULL DEFAULT 0,
    port_dst                   INT                         NOT NULL DEFAULT 0,
    ip_proto                   SMALLINT                    NOT NULL DEFAULT 0,
    packets                    INT                         NOT NULL,
    bytes                      BIGINT                      NOT NULL,
    stamp_inserted             timestamp without time zone NOT NULL DEFAULT '0001-01-01 00:00:00',
    stamp_updated              timestamp without time zone,
    country_ip_src             CHAR(2)                              DEFAULT NULL,
    country_ip_dst             CHAR(2)                              DEFAULT NULL,
    timestamp_arrival          timestamp without time zone,
    timestamp_arrival_residual int
);

CREATE INDEX idx_stamp_inserted ON collected(stamp_inserted);

DROP TABLE IF EXISTS metadata_status;
CREATE TABLE metadata_status
(
    pk                            SERIAL PRIMARY KEY,
    table_name                    VARCHAR(50)                     NOT NULL,
    latest_pk                     INT                             NOT NULL  
);

DROP TABLE IF EXISTS mergedata_status;
CREATE TABLE mergedata_status
(
    pk                            SERIAL PRIMARY KEY,
    source_table_name             VARCHAR(50)                    NOT NULL,
    destination_table_name        VARCHAR(50)                    NOT NULL,
    latest_pk                     INT                            NOT NULL  
);


DROP TABLE IF EXISTS collected_merged;
CREATE TABLE collected_merged
(
    pk                         SERIAL PRIMARY KEY,
    source_pk                  INT,
    mac_src                    macaddr                     NOT NULL DEFAULT '0:0:0:0:0:0',
    mac_dst                    macaddr                     NOT NULL DEFAULT '0:0:0:0:0:0',
    ip_src                     inet                        NOT NULL DEFAULT '0.0.0.0',
    ip_dst                     inet                        NOT NULL DEFAULT '0.0.0.0',
    port_src                   INT                         NOT NULL DEFAULT 0,
    port_dst                   INT                         NOT NULL DEFAULT 0,
    ip_proto                   SMALLINT                    NOT NULL DEFAULT 0,
    packets                    INT                         NOT NULL,
    bytes                      BIGINT                      NOT NULL,
    stamp_inserted             timestamp without time zone NOT NULL DEFAULT '0001-01-01 00:00:00',
    stamp_updated              timestamp without time zone,
    country_ip_src             CHAR(2)                              DEFAULT NULL,
    country_ip_dst             CHAR(2)                              DEFAULT NULL,
    timestamp_arrival          timestamp without time zone,
    timestamp_arrival_residual int,

    traffic_direction          SMALLINT                             DEFAULT NULL,
    whois_asn_country_code_src     VARCHAR(2)                           DEFAULT NULL,
    whois_asn_description_src      VARCHAR(256)                         DEFAULT NULL,
    fqdn_src                       VARCHAR(100)                         DEFAULT NULL,
    whois_asn_country_code_dst     VARCHAR(2)                           DEFAULT NULL,
    whois_asn_description_dst      VARCHAR(256)                         DEFAULT NULL,
    fqdn_dst                       VARCHAR(100)                         DEFAULT NULL
);


DROP TABLE IF EXISTS collected_1h_merged;
CREATE TABLE collected_1h_merged
(
    pk                         SERIAL PRIMARY KEY,
    source_pk                  INT,
    mac_src                    macaddr                     NOT NULL DEFAULT '0:0:0:0:0:0',
    mac_dst                    macaddr                     NOT NULL DEFAULT '0:0:0:0:0:0',
    ip_src                     inet                        NOT NULL DEFAULT '0.0.0.0',
    ip_dst                     inet                        NOT NULL DEFAULT '0.0.0.0',
    port_src                   INT                         NOT NULL DEFAULT 0,
    port_dst                   INT                         NOT NULL DEFAULT 0,
    ip_proto                   SMALLINT                    NOT NULL DEFAULT 0,
    packets                    INT                         NOT NULL,
    bytes                      BIGINT                      NOT NULL,
    stamp_inserted             timestamp without time zone NOT NULL DEFAULT '0001-01-01 00:00:00',
    stamp_updated              timestamp without time zone,
    country_ip_src             CHAR(2)                              DEFAULT NULL,
    country_ip_dst             CHAR(2)                              DEFAULT NULL,
    timestamp_arrival          timestamp without time zone,
    timestamp_arrival_residual int,

    traffic_direction          SMALLINT                             DEFAULT NULL,
    whois_asn_country_code_src     VARCHAR(2)                           DEFAULT NULL,
    whois_asn_description_src      VARCHAR(256)                         DEFAULT NULL,
    fqdn_src                       VARCHAR(100)                         DEFAULT NULL,
    whois_asn_country_code_dst     VARCHAR(2)                           DEFAULT NULL,
    whois_asn_description_dst      VARCHAR(256)                         DEFAULT NULL,
    fqdn_dst                       VARCHAR(100)                         DEFAULT NULL
);






DROP TABLE IF EXISTS fqdn;
CREATE TABLE fqdn
(
    pk                         SERIAL PRIMARY KEY,
    ip_address                 inet                        NOT NULL DEFAULT '0.0.0.0',
    stamp_updated              timestamp without time zone NOT NULL DEFAULT NOW(),
    fqdn                       VARCHAR(100)                         DEFAULT NULL
);

DROP TABLE IF EXISTS proto;
CREATE TABLE proto
(
    num         SMALLINT NOT NULL,
    description CHAR(20),
    CONSTRAINT proto_pk PRIMARY KEY (num)
);

COPY proto FROM stdin USING DELIMITERS ',';
0,ip
1,icmp
2,igmp
3,ggp
4,ipencap
5,st
6,tcp
8,egp
9,igp
17,udp
18,mux
27,rdp
29,iso-tp4
30,netblt
37,ddp
39,idpr-cmtp
41,ipv6
43,ipv6-route
44,ipv6-frag
46,rsvp
47,gre
50,ipv6-crypt
51,ipv6-auth
55,mobile
56,tlsp
58,ipv6-icmp
59,ipv6-nonxt
60,ipv6-opts
80,iso-ip
83,vines
88,eigrp
89,ospf
90,sprite-rpc
93,ax-25
94,ipip
98,encap
102,pnni
108,IPcomp
111,ipx-in-ip
112,vrrp
115,l2tp
124,isis
132,sctp
133,fc
\.



DROP TABLE IF EXISTS whois;
CREATE TABLE whois
(
    pk                    SERIAL PRIMARY KEY,
    stamp_updated         timestamp without time zone NOT NULL DEFAULT NOW(),

    asn_cidr              inet                                 DEFAULT NULL,
    asn_country_code      VARCHAR(2)                           DEFAULT NULL,
    asn_date              DATE                                 DEFAULT NULL,
    asn_description       VARCHAR(256)                         DEFAULT NULL,

    network_start_address inet                                 DEFAULT NULL, --#  '104.16.0.0',
    network_end_address   inet                                 DEFAULT NULL, --#  '104.31.255.255',
    network_ip_version    VARCHAR(2)                           DEFAULT NULL  --#  'v4'
);



INSERT INTO whois(asn_cidr, asn_country_code, asn_description, asn_date, stamp_updated)
VALUES
   ('10.0.0.0/8', '-', 'local', '0001-01-01 00:00:00', '0001-01-01 00:00:00'),
   ('172.16.0.0/12', '-', 'local', '0001-01-01 00:00:00', '0001-01-01 00:00:00'),
   ('192.168.0.0/16', '-', 'local', '0001-01-01 00:00:00', '0001-01-01 00:00:00'),
   ('fd00::/8', '-', 'local', '0001-01-01 00:00:00', '0001-01-01 00:00:00'),
   ('224.0.0/24', '-', 'local', '0001-01-01 00:00:00', '0001-01-01 00:00:00'),
   ('255.255.255.255', '-', 'local', '0001-01-01 00:00:00', '0001-01-01 00:00:00'),
   ('239.255.255.250', '-', 'local', '0001-01-01 00:00:00', '0001-01-01 00:00:00'),
   ('FF02::C', '-', 'local', '0001-01-01 00:00:00', '0001-01-01 00:00:00'),
   ('FF05::C', '-', 'local', '0001-01-01 00:00:00', '0001-01-01 00:00:00');

