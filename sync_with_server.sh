#!/usr/bin/env bash

function sync() {
    rsync \
    --checksum \
    --human-readable \
    --archive \
    --verbose \
    --compress \
    --partial \
    --progress \
    --stats \
    ${from} ${to} \
    --exclude-from ./rsync.exclude \
    --rsync-path "sudo rsync" # remote rsync path
}


function push() {
    from=${local_path}
    to=${remote_server}:${remote_path}
    echo "PUSH. target_path ${remote_path}"
    sync
}


USAGE="$0 user@host"

if [ -z "$1" ]; then
    echo "usage: $USAGE"
    exit 0
else
    DEV_SERVER=$1
    DEV_PATH=/home/pi/shfk/

    remote_server=${DEV_SERVER}
    remote_path=${DEV_PATH}

    local_path=./

    push
fi
shift



