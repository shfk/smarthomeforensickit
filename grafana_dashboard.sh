#!/usr/bin/env bash

set -x

source scripts/_grafana_sync.sh
source scripts/shfk.conf

function push() {
    local host="${1}"
    local path="${2}"


}

function pull() {
    echo todo
}


USAGE="$0 <host> [<args>]
ARGUMENTS
pull - pulls all dashboards to grafana/dashboards
push <path> - push dashboard at path to grafana. if no path is given, all dashboards in grafana/dahboards are pushed."

if [ -n "${1}" ]; then
    HOST="${1}"
else
    echo "${USAGE}"
    exit 0
fi
shift

if [ "$1" == "push" ]; then
    command="push"
elif [ "$1" == "pull" ]; then
    command="pull"
else
    echo "${USAGE}"
    exit 0
fi
shift


if [ "${command}" == "push" ]; then
    if [ -n "${1}" ]; then
        FILE_PATH=${1}
        _grafana_post_dashboard "${HOST}" "${GRAFANA_ADMIN_USER}" "${GRAFANA_ADMIN_PASSWORD}" "${FILE_PATH}"
    else
        # if no filename is given, post all boards in grafana/dashboards
        for f in grafana/dashboards/*
        do
           FILE_PATH=${f}
            _grafana_post_dashboard "${HOST}" "${GRAFANA_ADMIN_USER}" "${GRAFANA_ADMIN_PASSWORD}" "${FILE_PATH}"
        done
    fi
    _restore_main_dashboard "${HOST}" "${GRAFANA_ADMIN_USER}" "${GRAFANA_ADMIN_PASSWORD}"

elif [ "${command}" == "pull" ]; then
    _grafana_backup "${HOST}"  "${GRAFANA_ADMIN_USER}" "${GRAFANA_ADMIN_PASSWORD}"
else
    echo "${USAGE}"
    exit 0
fi



